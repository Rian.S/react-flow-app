import { Routes, Route } from 'react-router-dom'
import HomePage from './views/HomePage'
import CustomTextUpdaterNode from './views/CustomTextUpdaterNode'
import CustomColorSelectorNode from './views/CustomColorSelectorNode'
import CustomImageNode from './views/CustomImageNode'
import DragNDrop from './views/DragNDrop'

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/custom-text-updater-node" element={<CustomTextUpdaterNode />} />
        <Route path="/custom-color-selector-node" element={<CustomColorSelectorNode />} />
        <Route path="/custom-image-node" element={<CustomImageNode />} />
        <Route path="/dragndrop" element={<DragNDrop />} />
      </Routes>
    </div>
  );
}

export default App;
