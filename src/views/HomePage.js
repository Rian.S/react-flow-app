import React, { useCallback, useState } from 'react'
import ReactFlow, { 
    MiniMap,
    Controls,
    Background,
    useNodesState, 
    useEdgesState, 
    addEdge,
    applyNodeChanges,
    applyEdgeChanges,
} from 'reactflow'

import 'reactflow/dist/style.css'

const initialNodes = [
    // { id: '1', position: { x: 0, y: 0 }, data: { label: 'Hello' }, type: 'input' },
    { id: '1', position: { x: 0, y: 0 }, data: { label: 'Hello' } },
    { id: '2', position: { x: 0, y: 100 }, data: { label: 'World' } },
]

// const initialEdges = [{ id: 'e1-2', source: '1', target: '2', label: 'to the', type: 'step' }]
const initialEdges = []

export default function HomePage() {
    // const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes)
    // const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges)

    const [nodes, setNodes] = useState(initialNodes)
    const [edges, setEdges] = useState(initialEdges)

    const onNodesChange = useCallback(
        (changes) => {
            console.log('node changes: ', changes)
            return setNodes((nds) => {
                console.log('changed node: ', nds)
                return applyNodeChanges(changes, nds)
            })
        },
        []
    )

    const onEdgesChange = useCallback(
        (changes) => {
            console.log('edge changes: ', changes)
            return setEdges((eds) => {
                console.log('changed edge: ', eds)
                return applyEdgeChanges(changes, eds)
            })
        },
        []
    )

    const onConnect = useCallback(
        (connection) => {
            console.log('connect params: ', connection)
            return setEdges((eds) => {
                console.log('connected edge: ', eds)
                return addEdge({ ...connection }, eds)
            })
        }, 
        []
    )

    return (
        <div style={{ width: '100vw', height: '100vh' }}>
            <ReactFlow 
                nodes={nodes} 
                edges={edges} 
                onNodesChange={onNodesChange}
                onEdgesChange={onEdgesChange}
                onConnect={onConnect}
            >
                <Controls />
                <MiniMap />
                <Background variant='dots' gap={12} size={1} />
            </ReactFlow>
        </div>
    )
}