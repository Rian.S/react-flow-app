import React, { useState, useEffect, useCallback } from 'react';
import ReactFlow, {
    useNodesState,
    useEdgesState,
    addEdge,
    Controls,
    Background
  } from "reactflow";
import "reactflow/dist/style.css";

import ImageNode from "../components/customReactFlow/ImageNode"

// const connectionLineStyle = { stroke: "red" };
const snapGrid = [5, 5];
const nodeTypes = {
  imageNode: ImageNode
};
const defaultViewport = { x: 0, y: 0, zoom: 1.5 };
const proOptions = { hideAttribution: true };

const CustomImageNode = () => {
    const [nodes, setNodes, onNodesChange] = useNodesState([]);
    const [edges, setEdges, onEdgesChange] = useEdgesState([]);

    useEffect(() => {
        setNodes([
            {
                id: '1',
                type: 'imageNode',
                data: {
                    image: {
                        url: '/images/survey.svg',
                        alt: 'node 1'
                    }
                },
                position: { x: 50, y: 50 }
            },
            {
                id: '2',
                type: 'imageNode',
                data: {
                    image: {
                        url: '/images/survey.svg',
                        alt: 'node 2'
                    }
                },
                position: { x: 200, y: 150 },
                targetPosition: 'left'
            }
        ])

        // setEdges([
        //     {
        //         id: 'e1-2',
        //         source: '1',
        //         target: '2',
        //         style: { stroke: "blue" }

        //     }
        // ])
    }, [])

    const onConnect = useCallback(
        (params) =>
            setEdges((eds) => addEdge({ ...params }, eds)),
        []
    );

    return (
        <div style={{ width: '100vw', height: '100vh' }}>
            <ReactFlow
                nodes={nodes}
                edges={edges}
                onNodesChange={onNodesChange}
                onEdgesChange={onEdgesChange}
                onConnect={onConnect}
                nodeTypes={nodeTypes}
                // connectionLineStyle={connectionLineStyle}
                // snapToGrid={true}
                // snapGrid={snapGrid}
                // defaultViewport={defaultViewport}
                // attributionPosition="bottom-left"
                proOptions={proOptions}
            >
                <Controls />
                <Background variant='lines' />
            </ReactFlow>
        </div>
    )
}

export default CustomImageNode