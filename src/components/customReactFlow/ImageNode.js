import React, { memo } from 'react';
import { Handle, Position } from 'reactflow';
import './imageNode.scss'

export default memo(({ data, isConnectable }) => {
    return (
        <div className="custom-image-node">
            <Handle
                type="target"
                position={Position.Left}
                onConnect={(params) => console.log("handle onConnect", params)}
                isConnectable={isConnectable}
            />
            <img src={data.image.url} alt={data.image.alt} />
            <Handle
                type="source"
                position={Position.Right}
                id="a"
                isConnectable={isConnectable}
            />
        </div>
    )
})